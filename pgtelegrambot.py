import telebot
from telebot import TeleBot
import config
import random
from telebot import types
import psycopg2
from psycopg2 import Error
#return tennis toddler security pulse brief glimpse omit wrong task dignity craft

bot: TeleBot = telebot.TeleBot(config.TOKEN)
prev_message = " "
msgfromid = " "
uservalentine = " "

def user_registration(id, name, coins, status, bank, rating, box):
    try:
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT, database=config.DATABASE)
        cursor = connection.cursor()
        insert_query = """INSERT INTO users VALUES (%s, %s, %s, %s, %s, %s, %s)"""
        cursor.execute(insert_query, (id, name, coins, status, bank, rating, box,))
        connection.commit()
        #print("Запись успешно вставлена")
        cursor.execute("SELECT * from users")
        #record = cursor.fetchall()
        #print("Результат", record)
    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL, user_registration", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def user_delete(id):
    try:
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT, database=config.DATABASE)
        cursor = connection.cursor()
        delete_query = """ DELETE from users WHERE "USER_ID" = %s"""
        cursor.execute(delete_query, (id,))
        connection.commit()
        cursor.execute("SELECT * from users")
        #record = cursor.fetchall()
        #print("Результат", record)
    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL, user_Delete", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def user_update(id, new_name, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        #print("Результат", record)
        if len(new_name) >= 17:
            bot.send_message(send_to,
                             "К сожалению нельзя сменить ник длиннее 15 символов(",
                             parse_mode='html')
        if len(new_name) <= 5:
            bot.send_message(send_to,
                             "К сожалению нельзя сменить ник короче 5 символов(",
                             parse_mode='html')
        if record != []:
            if len(new_name) >= 6:
                if len(new_name) < 17:
                    update_query = """UPDATE users SET "USER_NAME" = %s WHERE "USER_ID" = %s"""
                    cursor.execute(update_query, (new_name, id,))
                    connection.commit()
                    new_name_query = """SELECT "USER_NAME" FROM users WHERE "USER_ID" = %s"""
                    cursor.execute(new_name_query, (id,))
                    record = cursor.fetchall()
                    #print("Результат", record)
                    for x in record:
                        user_name = str(x[0])
                        bot.send_message(send_to, "Ты успешно поменял ник на \"" + f"<a href='tg://user?id={id}'>{user_name}</a>" + "\"", parse_mode='html')
        else:
            bot.send_message(send_to, "Зарегестрируйся! (Команда: /start@patrikgame_bot)")

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL, user_update", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def userdata(id):
    try:
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT, database=config.DATABASE)
        cursor = connection.cursor()
        userdata_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(userdata_query, (id,))
        record = cursor.fetchall()
        #print("Результат", record)
        return record

    except(Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL, userdata", error)

    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def user_issue(id, name1, id_reply, coins, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        userproverkacoins(message.from_user.id)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            user_name = str(z[1])
            status = str(z[3])
            selectreply_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
            cursor.execute(selectreply_query, (id_reply,))
            record1 = cursor.fetchall()
            for y in record1:
                user_namereply = str(y[1])
                balance = int(y[2])
                if status == "Admin":
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно выдали " + str(coins) + " вареник(ов) игроку " + f"<a href='tg://user?id={id_reply}'>{user_namereply}</a>", parse_mode='html')
                    update_balance_query = f"""UPDATE users SET "USER_COINS" = {balance+coins} WHERE "USER_ID" = %s"""
                    cursor.execute(update_balance_query, (id_reply,))
                    connection.commit()
                    userproverkacoins(message.from_user.id)
                    #record2 = cursor.fetchall()
                    #print("Результат", record2)
                if status == "Creator":
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно выдали " + str(coins) + " вареник(ов) игроку " + f"<a href='tg://user?id={id_reply}'>{user_namereply}</a>", parse_mode='html')
                    update_balance_query = f"""UPDATE users SET "USER_COINS" = {balance+coins} WHERE "USER_ID" = %s"""
                    cursor.execute(update_balance_query, (id_reply,))
                    connection.commit()
                    userproverkacoins(message.from_user.id)
                    #record2 = cursor.fetchall()
                    #print("Результат", record2)
                if status == "Player":
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", к сожалению у вас недостаточно прав чтобы выдать вареники. За покупкой обращайтесь к создателю - @XxfloppaxX", parse_mode='html')
                    userproverkacoins(message.from_user.id)




    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в user_issue", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def userdeletecoins(id, name1, id_reply, coins, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            user_name = str(z[1])
            status = str(z[3])
            selectreply_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
            cursor.execute(selectreply_query, (id_reply,))
            record1 = cursor.fetchall()
            for y in record1:
                user_namereply = str(y[1])
                balance = int(y[2])
                if status == "Admin":
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно забрали " + str(coins) + " вареник(ов) у игрока " + f"<a href='tg://user?id={id_reply}'>{user_namereply}</a>", parse_mode='html')
                    update_balance_query = f"""UPDATE users SET "USER_COINS" = {balance-coins} WHERE "USER_ID" = %s"""
                    cursor.execute(update_balance_query, (id_reply,))
                    connection.commit()
                    #record2 = cursor.fetchall()
                    #print("Результат", record2)
                if status == "Creator":
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно забрали " + str(coins) + " вареник(ов) у игрока " + f"<a href='tg://user?id={id_reply}'>{user_namereply}</a>", parse_mode='html')
                    update_balance_query = f"""UPDATE users SET "USER_COINS" = {balance-coins} WHERE "USER_ID" = %s"""
                    cursor.execute(update_balance_query, (id_reply,))
                    connection.commit()
                    #record2 = cursor.fetchall()
                    #print("Результат", record2)
                if status == "Player":
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", к сожалению у вас недостаточно прав чтобы забрать вареники. За покупкой обращайтесь к создателю - @XxfloppaxX", parse_mode='html')

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в user_deletecoins", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def usernullcoins(id, name1, id_reply, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            user_name = str(z[1])
            status = str(z[3])
            selectreply_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
            cursor.execute(selectreply_query, (id_reply,))
            record1 = cursor.fetchall()
            for y in record1:
                user_namereply = str(y[1])
                if status == "Admin":
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно обнулили игрока " + f"<a href='tg://user?id={id_reply}'>{user_namereply}</a>", parse_mode='html')
                    update_balance_query = f"""UPDATE users SET "USER_COINS" = 0 WHERE "USER_ID" = %s"""
                    cursor.execute(update_balance_query, (id_reply,))
                    connection.commit()
                    update_bank_query = f"""UPDATE users SET "USER_BANKCOINS" = 0 WHERE "USER_ID" = %s"""
                    cursor.execute(update_bank_query, (id_reply,))
                    connection.commit()
                    update_rating_query = f"""UPDATE users SET "USER_RATING" = 0 WHERE "USER_ID" = %s"""
                    cursor.execute(update_rating_query, (id_reply,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if status == "Creator":
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно обнулили игрока " + f"<a href='tg://user?id={id_reply}'>{user_namereply}</a>", parse_mode='html')
                    update_balance_query = f"""UPDATE users SET "USER_COINS" = 0 WHERE "USER_ID" = %s"""
                    cursor.execute(update_balance_query, (id_reply,))
                    connection.commit()
                    update_bank_query = f"""UPDATE users SET "USER_BANKCOINS" = 0 WHERE "USER_ID" = %s"""
                    cursor.execute(update_bank_query, (id_reply,))
                    connection.commit()
                    update_rating_query = f"""UPDATE users SET "USER_RATING" = 0 WHERE "USER_ID" = %s"""
                    cursor.execute(update_rating_query, (id_reply,))
                    connection.commit()
                    update_rating_query = f"""UPDATE users SET "USER_CASE" = 0 WHERE "USER_ID" = %s"""
                    cursor.execute(update_rating_query, (id_reply,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if status == "Player":
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", к сожалению у вас недостаточно прав чтобы обнулить игрока. За покупкой обращайтесь к создателю - @XxfloppaxX", parse_mode='html')

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в usernull", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")

def uservarkacoins(id, coins, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            user_name = str(z[1])
            balance = int(z[2])
            name = str(z[1])
            winpercentace = int(random.randint(0,54))
            if coins == "all":
                if winpercentace in range(0, 7):
                    update_x0_query = f"""UPDATE users SET "USER_COINS" = {balance - balance} WHERE "USER_ID" = %s"""
                    bot.send_message(send_to,
                                     f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени переварились и протухли, а сметана прокисла. К сожалению вы проигрываете всю сумму заказа( (x0)",
                                     parse_mode='html')
                    cursor.execute(update_x0_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if winpercentace in range(8, 15):
                    update_x05_query = f"""UPDATE users SET "USER_COINS" = {balance - balance/2} WHERE "USER_ID" = %s"""
                    bot.send_message(send_to,
                                     f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени долго варились, и вы не заметили как они переварились. К сожалению вы проигрываете половину суммы заказа( (x0.5)",
                                     parse_mode='html')
                    cursor.execute(update_x05_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if winpercentace in range(16, 27):
                    update_x1_query = f"""UPDATE users SET "USER_COINS" = {balance} WHERE "USER_ID" = %s"""
                    bot.send_message(send_to,
                                     f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени нормальные, как обычно. Вы не теряете и не получаете вареники (x1)",
                                     parse_mode='html')
                    cursor.execute(update_x1_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if winpercentace in range(28, 39):
                    update_x15_query = f"""UPDATE users SET "USER_COINS" = {balance + balance * 0.5} WHERE "USER_ID" = %s"""
                    bot.send_message(send_to,
                                     f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени неплохие, покупателю понравилось. Вы получаете чаевые - половину от суммы заказа! (x1.5)",
                                     parse_mode='html')
                    cursor.execute(update_x15_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if winpercentace in range(40, 48):
                    update_x2_query = f"""UPDATE users SET "USER_COINS" = {balance + balance} WHERE "USER_ID" = %s"""
                    bot.send_message(send_to,
                                     f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени вкусные, покупателю очень понравилось. Вы получаете чаевые - всю сумму заказа! (x2)",
                                     parse_mode='html')
                    cursor.execute(update_x2_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if winpercentace in range(49, 53):
                    update_x3_query = f"""UPDATE users SET "USER_COINS" = {balance + balance * 2} WHERE "USER_ID" = %s"""
                    bot.send_message(send_to,
                                     f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени очень вкусные, покупатель в восторге. Вы получаете большие чаевые - в 2 раза больше суммы заказа! (x3)",
                                     parse_mode='html')
                    cursor.execute(update_x3_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if winpercentace == 54:
                    update_x5_query = f"""UPDATE users SET "USER_COINS" = {balance + balance * 4} WHERE "USER_ID" = %s"""
                    bot.send_message(send_to,
                                     f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени божественные, покупатель просит добавки. Вы получаете очень большие чаевые - в 4 раза больше суммы заказа! (x5)",
                                     parse_mode='html')
                    cursor.execute(update_x5_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
            if coins != "all":
                if balance < coins:
                    bot.send_message(send_to,
                                     f"<a href='tg://user?id={id}'>{user_name}</a>" + ", у вас недостаточно вареников на балансе(",
                                     parse_mode='html')
                if coins <= 0:
                    bot.send_message(send_to,
                                     f"<a href='tg://user?id={id}'>{user_name}</a>" + ", нельзя поставить отрицательное число(",
                                     parse_mode='html')
                if balance >= coins:
                    if coins > 0:
                        if winpercentace in range(0, 7):
                            update_x0_query = f"""UPDATE users SET "USER_COINS" = {balance - coins} WHERE "USER_ID" = %s"""
                            bot.send_message(send_to,
                                             f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени переварились и протухли, а сметана прокисла. К сожалению вы проигрываете всю сумму заказа( (x0)",
                                             parse_mode='html')
                            cursor.execute(update_x0_query, (id,))
                            connection.commit()
                            # record2 = cursor.fetchall()
                            # print("Результат", record2)
                        if winpercentace in range(8, 15):
                            update_x05_query = f"""UPDATE users SET "USER_COINS" = {balance - coins / 2} WHERE "USER_ID" = %s"""
                            bot.send_message(send_to,
                                             f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени долго варились, и вы не заметили как они переварились. К сожалению вы проигрываете половину суммы заказа( (x0.5)",
                                             parse_mode='html')
                            cursor.execute(update_x05_query, (id,))
                            connection.commit()
                            # record2 = cursor.fetchall()
                            # print("Результат", record2)
                        if winpercentace in range(16, 27):
                            update_x1_query = f"""UPDATE users SET "USER_COINS" = {balance} WHERE "USER_ID" = %s"""
                            bot.send_message(send_to,
                                             f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени нормальные, как обычно. Вы не теряете и не получаете вареники (x1)",
                                             parse_mode='html')
                            cursor.execute(update_x1_query, (id,))
                            connection.commit()
                            # record2 = cursor.fetchall()
                            # print("Результат", record2)
                        if winpercentace in range(28, 39):
                            update_x15_query = f"""UPDATE users SET "USER_COINS" = {balance + coins * 0.5} WHERE "USER_ID" = %s"""
                            bot.send_message(send_to,
                                             f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени неплохие, покупателю понравилось. Вы получаете чаевые - половину от суммы заказа! (x1.5)",
                                             parse_mode='html')
                            cursor.execute(update_x15_query, (id,))
                            connection.commit()
                            # record2 = cursor.fetchall()
                            # print("Результат", record2)
                        if winpercentace in range(40, 48):
                            update_x2_query = f"""UPDATE users SET "USER_COINS" = {balance + coins} WHERE "USER_ID" = %s"""
                            bot.send_message(send_to,
                                             f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени вкусные, покупателю очень понравилось. Вы получаете чаевые - всю сумму заказа! (x2)",
                                             parse_mode='html')
                            cursor.execute(update_x2_query, (id,))
                            connection.commit()
                            # record2 = cursor.fetchall()
                            # print("Результат", record2)
                        if winpercentace in range(49, 53):
                            update_x3_query = f"""UPDATE users SET "USER_COINS" = {balance + coins * 2} WHERE "USER_ID" = %s"""
                            bot.send_message(send_to,
                                             f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени очень вкусные, покупатель в восторге. Вы получаете большие чаевые - в 2 раза больше суммы заказа! (x3)",
                                             parse_mode='html')
                            cursor.execute(update_x3_query, (id,))
                            connection.commit()
                            # record2 = cursor.fetchall()
                            # print("Результат", record2)
                        if winpercentace == 54:
                            update_x5_query = f"""UPDATE users SET "USER_COINS" = {balance + coins * 4} WHERE "USER_ID" = %s"""
                            bot.send_message(send_to,
                                             f"<a href='tg://user?id={id}'>{user_name}</a>" + ", ваши пельмени божественные, покупатель просит добавки. Вы получаете очень большие чаевые - в 4 раза больше суммы заказа! (x5)",
                                             parse_mode='html')
                            cursor.execute(update_x5_query, (id,))
                            connection.commit()
                            # record2 = cursor.fetchall()
                            # print("Результат", record2)



    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в uservarka", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def userbankcoins(id, coins, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            user_name = str(z[1])
            balance_bank = int(z[4])
            balance = int(z[2])
            if balance < coins:
                bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", к сожалению у вас не хватает средств на счету чтобы положить их в пельменную", parse_mode='html')
            if coins <= 0:
                bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", нельзя класть в пельменную отрицательное число!", parse_mode='html')
            if balance >= coins:
                if coins > 0:
                    update_balance_query = f"""UPDATE users SET "USER_COINS" = {balance-coins} WHERE "USER_ID" = %s"""
                    cursor.execute(update_balance_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    update_bank_query = f"""UPDATE users SET "USER_BANKCOINS" = {balance_bank + coins} WHERE "USER_ID" = %s"""
                    cursor.execute(update_bank_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно положили в пельменную " + str(coins) + " вареников.", parse_mode='html')



    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в userbank+", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def userbankdeletecoins(id, coins, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            user_name = str(z[1])
            balance_bank = int(z[4])
            balance = int(z[2])
            if balance_bank < coins:
                bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", к сожалению у вас не хватает средств в пельменной чтобы их снять", parse_mode='html')
            if coins <= 0:
                bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", нельзя снимать из пельменной отрицательное число!", parse_mode='html')
            if balance_bank >= coins:
                if coins > 0:
                    update_balance_query = f"""UPDATE users SET "USER_BANKCOINS" = {balance_bank-coins} WHERE "USER_ID" = %s"""
                    cursor.execute(update_balance_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    update_bank_query = f"""UPDATE users SET "USER_COINS" = {balance + coins} WHERE "USER_ID" = %s"""
                    cursor.execute(update_bank_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно сняли из пельменной " + str(coins) + " вареников.", parse_mode='html')


    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в userbank-", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def userproverkacoins(id):
    try:
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            balance = int(z[2])
            if balance >= 999999999999999999:
                update_coins_query = f"""UPDATE users SET "USER_COINS" = 999999999999999999 WHERE "USER_ID" = %s"""
                cursor.execute(update_coins_query, (id,))
                connection.commit()
                # record2 = cursor.fetchall()
                # print("Результат", record2)

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в userproverka", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def useradmin(id, name1, id_reply, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            user_name = str(z[1])
            status = str(z[3])
            selectreply_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
            cursor.execute(selectreply_query, (id_reply,))
            record1 = cursor.fetchall()
            for y in record1:
                user_namereply = str(y[1])
                status_reply = str(y[3])
                if status == "Creator":
                    if status_reply == "Player":
                        bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно повысили игрока " + f"<a href='tg://user?id={id_reply}'>{user_namereply}</a>" + " до статуса \"Admin\"", parse_mode='html')
                        update_admin_query = f"""UPDATE users SET "USER_STATUS" = %s WHERE "USER_ID" = %s"""
                        cursor.execute(update_admin_query, ("Admin", id_reply,))
                        connection.commit()
                    #record2 = cursor.fetchall()
                    #print("Результат", record2)
                    if status_reply == "Admin":
                        bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", игрок которого вы хотите повысить уже админ", parse_mode='html')
                if status == "Admin":
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", эту команду может использовать только создатель бота!", parse_mode='html')
                if status == "Player":
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", эту команду может использовать только создатель бота!", parse_mode='html')

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в useradmin", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def userdeleteadmin(id, name1, id_reply, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            user_name = str(z[1])
            status = str(z[3])
            selectreply_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
            cursor.execute(selectreply_query, (id_reply,))
            record1 = cursor.fetchall()
            for y in record1:
                status_reply = str(y[3])
                user_namereply = str(y[1])
                if status == "Creator":
                    if status_reply == "Admin":
                        bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно понизили игрока " + f"<a href='tg://user?id={id_reply}'>{user_namereply}</a>" + " до статуса \"Player\"", parse_mode='html')
                        update_balance_query = f"""UPDATE users SET "USER_STATUS" = %s WHERE "USER_ID" = %s"""
                        cursor.execute(update_balance_query, ("Player", id_reply,))
                        connection.commit()
                    #record2 = cursor.fetchall()
                    #print("Результат", record2)
                    if status_reply == "Player":
                        bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", игрок которого вы хотите понизить - игрок", parse_mode='html')
                if status == "Admin":
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", эту команду может использовать только создатель бота!", parse_mode='html')
                if status == "Player":
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", эту команду может использовать только создатель бота!", parse_mode='html')

    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в usernulladmin", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def userrating(id, rating, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            user_name = str(z[1])
            balance_rating = int(z[5])
            balance = int(z[2])
            newratingcoins = int(rating*150000)
            if balance < newratingcoins:
                bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", к сожалению у вас не хватает средств на счету чтобы купить рейтинг(", parse_mode='html')
            if newratingcoins <= 0:
                bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", нельзя покупать отрицательное число!", parse_mode='html')
            if balance >= newratingcoins:
                if newratingcoins > 0:
                    update_balance_query = f"""UPDATE users SET "USER_COINS" = {balance-newratingcoins} WHERE "USER_ID" = %s"""
                    cursor.execute(update_balance_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    updaterating_query = f"""UPDATE users SET "USER_RATING" = {balance_rating + rating} WHERE "USER_ID" = %s"""
                    cursor.execute(updaterating_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно купили " + str(rating) + " рейтинга.", parse_mode='html')



    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в userrating", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def userdeleterating(id, rating, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            user_name = str(z[1])
            balance_rating = int(z[5])
            balance = int(z[2])
            newratingcoins = int(rating*50000)
            if balance_rating < rating:
                bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", к сожалению у вас не хватает рейтинга чтобы его продать(", parse_mode='html')
            if newratingcoins <= 0:
                bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", нельзя продавать отрицательное число!", parse_mode='html')
            if balance_rating >= rating:
                if newratingcoins > 0:
                    update_balance_query = f"""UPDATE users SET "USER_COINS" = {balance+newratingcoins} WHERE "USER_ID" = %s"""
                    cursor.execute(update_balance_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    updaterating_query = f"""UPDATE users SET "USER_RATING" = {balance_rating - rating} WHERE "USER_ID" = %s"""
                    cursor.execute(updaterating_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно продали " + str(rating) + " рейтинга.", parse_mode='html')



    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в userdeleterating"
              "", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def userboxes(id, box, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            user_name = str(z[1])
            balance = int(z[2])
            boxmoney = int(box*10000)
            allboxes = int(z[6])
            if balance < boxmoney:
                bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", к сожалению у вас не хватает средств на счету чтобы купить кейсы(", parse_mode='html')
            if boxmoney <= 0:
                bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", нельзя покупать отрицательное число!", parse_mode='html')
            if balance >= boxmoney:
                if boxmoney > 0:
                        update_balance_query = f"""UPDATE users SET "USER_COINS" = {balance-boxmoney} WHERE "USER_ID" = %s"""
                        cursor.execute(update_balance_query, (id,))
                        connection.commit()
                        # record2 = cursor.fetchall()
                        # print("Результат", record2)
                        updatebox_query = f"""UPDATE users SET "USER_CASE" = {allboxes + box} WHERE "USER_ID" = %s"""
                        cursor.execute(updatebox_query, (id,))
                        connection.commit()
                        # record2 = cursor.fetchall()
                        # print("Результат", record2)
                        bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно купили " + str(box) + " кейсов.", parse_mode='html')




    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в usercase", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")


def useropenboxes(id, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST, port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            user_coins = int(z[2])
            user_name = str(z[1])
            allboxes = int(z[6])
            boxrange = int(random.randint(1,100))
            user_rating = int(z[5])
            if allboxes < 1:
                bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", к сожалению у вас нет кейсов.", parse_mode='html')
            if allboxes >= 1:
                if boxrange in range(1,45):
                    updatebox_query = f"""UPDATE users SET "USER_CASE" = {allboxes - 1} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", к сожалению в кейсе ничего нету.", parse_mode='html')
                if boxrange in range(46,56):
                    updatebox_query = f"""UPDATE users SET "USER_CASE" = {allboxes - 1} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", из кейса выпало 5000 вареников!", parse_mode='html')
                    updatebox_query = f"""UPDATE users SET "USER_COINS" = {user_coins + 5000} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if boxrange in range(57,66):
                    updatebox_query = f"""UPDATE users SET "USER_CASE" = {allboxes - 1} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", из кейса выпало 7500 вареников!", parse_mode='html')
                    updatebox_query = f"""UPDATE users SET "USER_COINS" = {user_coins + 7500} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if boxrange in range(67,76):
                    updatebox_query = f"""UPDATE users SET "USER_CASE" = {allboxes - 1} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", из кейса выпало 10000 вареников!", parse_mode='html')
                    updatebox_query = f"""UPDATE users SET "USER_COINS" = {user_coins + 10000} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if boxrange in range(76,82):
                    updatebox_query = f"""UPDATE users SET "USER_CASE" = {allboxes - 1} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", из кейса выпало 12500 вареников!", parse_mode='html')
                    updatebox_query = f"""UPDATE users SET "USER_COINS" = {user_coins + 12500} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if boxrange in range(83,87):
                    updatebox_query = f"""UPDATE users SET "USER_CASE" = {allboxes - 1} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", из кейса выпало 15000 вареников!", parse_mode='html')
                    updatebox_query = f"""UPDATE users SET "USER_COINS" = {user_coins + 15000} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if boxrange in range(88,91):
                    updatebox_query = f"""UPDATE users SET "USER_CASE" = {allboxes - 1} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", из кейса выпало 20000 вареников!", parse_mode='html')
                    updatebox_query = f"""UPDATE users SET "USER_COINS" = {user_coins + 20000} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if boxrange in range(92,94):
                    updatebox_query = f"""UPDATE users SET "USER_CASE" = {allboxes - 1} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", из кейса выпало 25000 вареников!", parse_mode='html')
                    updatebox_query = f"""UPDATE users SET "USER_COINS" = {user_coins + 25000} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if boxrange in range(95,97):
                    updatebox_query = f"""UPDATE users SET "USER_CASE" = {allboxes - 1} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", из кейса выпал 1 рейтинг!", parse_mode='html')
                    updatebox_query = f"""UPDATE users SET "USER_RATING" = {user_rating + 1} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                if boxrange == 98:
                    updatebox_query = f"""UPDATE users SET "USER_CASE" = {allboxes - 1} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", из кейса выпало 3 рейтинга!", parse_mode='html')
                    updatebox_query = f"""UPDATE users SET "USER_RATING" = {user_rating + 3} WHERE "USER_ID" = %s"""
                    cursor.execute(updatebox_query, (id,))
                    connection.commit()
                    # record2 = cursor.fetchall()
                    # print("Результат", record2)



    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в usercase", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            #print("Соединение с PostgreSQL закрыто")






def peredacha(id, money, message, id_reply):
        try:
            send_to = message.chat.id
            connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST,
                                          port=config.PORT,
                                          database=config.DATABASE)
            cursor = connection.cursor()
            select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
            cursor.execute(select_query, (id,))
            record = cursor.fetchall()
            for z in record:
                user_name = str(z[1])
                balance = int(z[2])
                selectreply_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
                cursor.execute(selectreply_query, (id_reply,))
                record1 = cursor.fetchall()
                for y in record1:
                    user_namereply = str(y[1])
                    balance_reply = int(y[2])
                    if balance < money:
                        bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", у вас не хватает денег на передачу вареников.", parse_mode='html')
                    if money <= 0:
                        bot.send_message(send_to,
                                         f"<a href='tg://user?id={id}'>{user_name}</a>" + ", к сожалению нельзя передавать 0 или меньше вареников.", parse_mode='html')
                    if id == id_reply:
                        bot.send_message(send_to,
                                         f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы не можете передать вареники самому себе!",
                                         parse_mode='html')
                    if id_reply == " ":
                        bot.send_message(send_to,
                                         f"<a href='tg://user?id={id}'>{user_name}</a>" + ", введите айди человека которому вы хотите передать вареники!",
                                         parse_mode='html')
                    if balance >= money:
                        if money > 0:
                            if id != id_reply:
                                bot.send_message(send_to,
                                             f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы успешно передали " + str(money) + f" вареников игроку <a href='tg://user?id={id_reply}'>{user_namereply}</a>",
                                             parse_mode='html')
                                update_admin_query = f"""UPDATE users SET "USER_COINS" = {balance-money} WHERE "USER_ID" = %s"""
                                cursor.execute(update_admin_query, (id,))
                                connection.commit()
                                update_balancereply_query = f"""UPDATE users SET "USER_COINS" = {balance_reply+money} WHERE "USER_ID" = %s"""
                                cursor.execute(update_balancereply_query, (id_reply,))
                                connection.commit()
                            # record2 = cursor.fetchall()
                            # print("Результат", record2)

        except (Exception, Error) as error:
            print("Ошибка при работе с PostgreSQL в usercase", error)
        finally:
            if connection:
                cursor.close()
                connection.close()
                #print("Соединение с PostgreSQL закрыто")


def promocodes(id, promocode, message):
    try:
        send_to = message.chat.id
        connection = psycopg2.connect(user=config.USER, password=config.PASSWORD, host=config.HOST,
                                      port=config.PORT,
                                      database=config.DATABASE)
        cursor = connection.cursor()
        select_query = """SELECT * FROM users WHERE "USER_ID" = %s"""
        cursor.execute(select_query, (id,))
        record = cursor.fetchall()
        for z in record:
            balance = int(z[2])
            user_name = str(z[1])
            userpromocodefriday = int(z[7])
            if promocode == "valentine":
                            bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", этот промокод больше не рабочий!",
                                     parse_mode='html')
            if promocode == "friday20":
                if userpromocodefriday == 0:
                    update_admin_query = f"""UPDATE users SET "USER_COINS" = {balance + 20000} WHERE "USER_ID" = %s"""
                    cursor.execute(update_admin_query, (id,))
                    connection.commit()
                    update_admin_query = f"""UPDATE users SET "USER_PROMOCODEFRIDAY" = 1 WHERE "USER_ID" = %s"""
                    cursor.execute(update_admin_query, (id,))
                    connection.commit()
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", успешно введён промокод friday20! Получено 20000 вареников!",
                                     parse_mode='html')
                if userpromocodefriday == 1:
                    bot.send_message(send_to, f"<a href='tg://user?id={id}'>{user_name}</a>" + ", вы уже вводили промокод friday20(",
                                     parse_mode='html')
            else:
                bot.send_message(send_to,
                                     f"<a href='tg://user?id={id}'>{user_name}</a>" + ", неправильно введён промокод!",
                                     parse_mode='html')


    except (Exception, Error) as error:
        print("Ошибка при работе с PostgreSQL в usercase", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            # print("Соединение с PostgreSQL закрыто")



@bot.message_handler(content_types=['text', 'document', 'audio'])
@bot.message_handler(commands=['switch'])
def get_text_messages(message):
    global prev_message
    global msgfromid
    send_to = message.chat.id
    time = [" секунд", " минут", " часов", " дней", " недель", " месяцев", " лет", " веков", " тысячелетий"]
    time2 = [" сегодня", " завтра", " послезавтра", " через неделю", " через год", " через месяц", " через век", " через тысячелетие", " через секунду", " через минуту", " через час", " никогда"]
    thought = ["\nДумаю " + "через " + str(random.randint(1, 60)) + str(random.choice(time)), "\nДумаю" + str(random.choice(time2))]
    #стартовая команда
    if message.text == "/start":
        user_registration(message.from_user.id, "Смените ник", str("500"), "Player", str("0"), str("0"), str("0"))
        bot.send_photo(send_to, open('hello.webp', 'rb'))
        bot.send_message(send_to, "Привет @" + str(message.from_user.username) + "! Mеня зовут PGBot. Рад тебя видеть! Тебе было бесплатно выдано 500 вареников!")
    if message.text == "/start@patrikgame_bot":
        user_registration(message.from_user.id, "Смените ник", str("500"), "Player", str("0"), str("0"), str("0"))
        bot.send_photo(send_to, open('hello.webp', 'rb'))
        bot.send_message(send_to, "Привет @" + str(message.from_user.username) + "! Mеня зовут PGBot. Рад тебя видеть! Тебе было бесплатно выдано 500 вареников!")
    if message.text == "регистрация":
        user_registration(message.from_user.id, "Смените ник", str("500"), "Player", str("0"), str("0"), str("0"))
        bot.send_photo(send_to, open('hello.webp', 'rb'))
        bot.send_message(send_to, "Привет @" + str(message.from_user.username) + "! Mеня зовут PGBot. Рад тебя видеть! Тебе было бесплатно выдано 500 вареников!")
    if message.text == "Регистрация":
        user_registration(message.from_user.id, "Смените ник", str("500"), "Player", str("0"), str("0"), str("0"))
        bot.send_photo(send_to, open('hello.webp', 'rb'))
        bot.send_message(send_to, "Привет @" + str(message.from_user.username) + "! Mеня зовут PGBot. Рад тебя видеть! Тебе было бесплатно выдано 500 вареников!")
    #рандом команды
    elif message.text.startswith ("Насколько"):
        userproverkacoins(message.from_user.id)
        bot.send_message(send_to, "@" + str(message.from_user.username) + "\nДумаю на " + str(random.randint(0, 100)) + "%")
    elif message.text.startswith ("насколько"):
        userproverkacoins(message.from_user.id)
        bot.send_message(send_to, "@" + str(message.from_user.username) + "\nДумаю на " + str(random.randint(0, 100)) + "%")
    elif message.text.startswith ("когда"):
        userproverkacoins(message.from_user.id)
        bot.send_message(send_to, "@" + str(message.from_user.username) + str(random.choice(thought)))
    elif message.text.startswith("Когда"):
        userproverkacoins(message.from_user.id)
        bot.send_message(send_to, "@" + str(message.from_user.username) + str(random.choice(thought)))
    elif message.text.startswith("шанс"):
        userproverkacoins(message.from_user.id)
        bot.send_message(send_to, "@" + str(message.from_user.username) + "\nШанс этого " + str(random.randint(0, 100)) + "%")
    elif message.text.startswith("Шанс"):
        userproverkacoins(message.from_user.id)
        bot.send_message(send_to, "@" + str(message.from_user.username) + "\nШанс этого " + str(random.randint(0, 100)) + "%")
    #команды связанные с основными функциями базы данных
    elif message.text.lower() in ["Удалиться", "удалиться"]:
        userproverkacoins(message.from_user.id)
        user_delete(message.from_user.id)
        bot.send_message(send_to, "@" + str(message.from_user.username) + ", ты успешно удалён из базы данных! " + "Но учти что твой прогресс будет удалён!")
    elif message.text.startswith("Сменить ник"):
        name = message.text
        user_update(message.from_user.id, name.replace("Сменить ник ", " "), message)
    elif message.text.startswith("сменить ник"):
        name = message.text
        user_update(message.from_user.id, name.replace("сменить ник ", " "), message)
    #команды связанные с варениками
    elif message.text.startswith("Выдать"):
        userproverkacoins(message.from_user.id)
        user_issue(message.from_user.id, message.from_user.username, message.reply_to_message.from_user.id, int(message.text.split()[1]), message)
    elif message.text.startswith("Забрать"):
        userproverkacoins(message.from_user.id)
        userdeletecoins(message.from_user.id, message.from_user.username, message.reply_to_message.from_user.id, int(message.text.split()[1]), message)
    elif message.text.startswith("выдать"):
        userproverkacoins(message.from_user.id)
        user_issue(message.from_user.id, message.from_user.username, message.reply_to_message.from_user.id, int(message.text.split()[1]), message)
        userproverkacoins(message.from_user.id)
    elif message.text.startswith("забрать"):
        userproverkacoins(message.from_user.id)
        userdeletecoins(message.from_user.id, message.from_user.username, message.reply_to_message.from_user.id, int(message.text.split()[1]), message)
        userproverkacoins(message.from_user.id)
    elif message.text.lower() in ["Обнулить", "обнулить"]:
        userproverkacoins(message.from_user.id)
        usernullcoins(message.from_user.id, message.from_user.username, message.reply_to_message.from_user.id, message)
        userproverkacoins(message.from_user.id)
    elif message.text.startswith("Сварить пельмени"):
        if message.text.split()[2] == "всё":
            uservarkacoins(message.from_user.id, str("all"), message)
        elif message.text.split()[2] == "Всё":
            uservarkacoins(message.from_user.id, str("all"), message)
        else:
            uservarkacoins(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("сварить пельмени"):
        if message.text.split()[2] == "всё":
            uservarkacoins(message.from_user.id, str("all"), message)
        elif message.text.split()[2] == "Всё":
            uservarkacoins(message.from_user.id, str("all"), message)
        else:
            uservarkacoins(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("Сварить Пельмени"):
        if message.text.split()[2] == "всё":
            uservarkacoins(message.from_user.id, str("all"), message)
        elif message.text.split()[2] == "Всё":
            uservarkacoins(message.from_user.id, str("all"), message)
        else:
            uservarkacoins(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("сварить Пельмени"):
        if message.text.split()[2] == "всё":
            uservarkacoins(message.from_user.id, str("all"), message)
        elif message.text.split()[2] == "Всё":
            uservarkacoins(message.from_user.id, str("all"), message)
        else:
            uservarkacoins(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("Банк положить"):
        userbankcoins(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("банк положить"):
        userbankcoins(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("Банк Положить"):
        userbankcoins(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("банк Положить"):
        userbankcoins(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("Банк снять"):
        userbankdeletecoins(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("банк снять"):
        userbankdeletecoins(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("Банк Снять"):
        userbankdeletecoins(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("банк Снять"):
        userbankdeletecoins(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("Купить рейтинг"):
        userrating(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("купить рейтинг"):
        userrating(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("Купить Рейтинг"):
        userrating(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("купить Рейтинг"):
        userrating(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("Продать рейтинг"):
        userdeleterating(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("продать рейтинг"):
        userdeleterating(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("Продать Рейтинг"):
        userdeleterating(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("продать Рейтинг"):
        userdeleterating(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("Купить кейс"):
        userboxes(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("Купить Кейс"):
        userboxes(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("купить кейс"):
        userboxes(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("купить Кейс"):
        userboxes(message.from_user.id, int(message.text.split()[2]), message)
    elif message.text.startswith("Дать"):
        if not message.reply_to_message:
            ub = userdata(message.from_user.id)
            for z in ub:
                user_name = str(z[1])
                bot.send_message(send_to, f"<a href='tg://user?id={message.from_user.id}'>{user_name}</a>" + ", эта команда должна быть ответом на сообщение!", parse_mode='html')
        else:
            peredacha(message.from_user.id, int(message.text.split()[1]), message, message.reply_to_message.from_user.id)
    elif message.text.startswith("дать"):
        if not message.reply_to_message:
            ub = userdata(message.from_user.id)
            for z in ub:
                user_name = str(z[1])
                bot.send_message(send_to, f"<a href='tg://user?id={message.from_user.id}'>{user_name}</a>" + ", эта команда должна быть ответом на сообщение!", parse_mode='html')
        else:
            peredacha(message.from_user.id, int(message.text.split()[1]), message, message.reply_to_message.from_user.id)
    elif message.text.startswith("Передать"):
            peredacha(message.from_user.id, int(message.text.split()[1]), message, int(message.text.split()[2]))
    elif message.text.startswith("передать"):
        peredacha(message.from_user.id, int(message.text.split()[1]), message, int(message.text.split()[2]))
    elif message.text.startswith("Промокод"):
        promocodes(message.from_user.id, str(message.text.split()[1]), message)
    elif message.text.startswith("промокод"):
        promocodes(message.from_user.id, str(message.text.split()[1]), message)
    elif message.text.lower() in ["Открыть кейс", "открыть кейс", "Открыть Кейс", "открыть Кейс"]:
        useropenboxes(message.from_user.id, message)
    #команды связанные со статистикой
    elif message.text.lower() in ["Повысить", "повысить"]:
        useradmin(message.from_user.id, message.from_user.username, message.reply_to_message.from_user.id, message)
    elif message.text.lower() in ["Понизить", "понизить"]:
        userdeleteadmin(message.from_user.id, message.from_user.username, message.reply_to_message.from_user.id, message)
    elif message.text.lower() in ["Баланс", "баланс", "Б", "б",]:
        ub = userdata(message.from_user.id)
        for z in ub:
            user_name = str(z[1])
            bot.send_message(send_to, f"<a href='tg://user?id={message.from_user.id}'>{user_name}</a>" + ", ваш баланс:" + "\n💰 Вареников: " + str(z[2]) + "\n🏦 В пельменной: " + str(z[4]) + "\n👑 Рейтинга: " + str(z[5]), parse_mode='html')
            userproverkacoins(message.from_user.id)
    elif message.text.lower() in ["Банк", "банк"]:
        ub = userdata(message.from_user.id)
        for z in ub:
            user_name = str(z[1])
            bot.send_message(send_to, f"<a href='tg://user?id={message.from_user.id}'>{user_name}</a>" + ", средств в банке:" + "\n💰 Вареников: " + str(z[4]), parse_mode='html')
            userproverkacoins(message.from_user.id)
    elif message.text.lower() in ["Айди", "айди"]:
        if not message.reply_to_message:
            ub = userdata(message.from_user.id)
            for z in ub:
                user_name = str(z[1])
                user_id = str(z[0])
                userproverkacoins(message.from_user.id)
                bot.send_message(send_to, f"<a href='tg://user?id={message.from_user.id}'>{user_name}</a>" + ", ваш айди: " + str(user_id), parse_mode='html')
        else:
            ub = userdata(message.reply_to_message.from_user.id)
            for z in ub:
                ud = userdata(message.from_user.id)
                for d in ud:
                    user_namereply = str(z[1])
                    user_idreply = str(z[0])
                    user_name = str(d[1])
                    user_id = int(d[0])
                    userproverkacoins(message.from_user.id)
                    bot.send_message(send_to,
                                 f"<a href='tg://user?id={user_id}'>{user_name}</a>" + ", айди игрока " + f"<a href='tg://user?id={message.reply_to_message.from_user.id}'>{user_namereply}</a>: " + str(user_idreply), parse_mode='html')
    elif message.text.lower() in ["Рейтинг", "рейтинг"]:
        ub = userdata(message.from_user.id)
        for z in ub:
            user_name = str(z[1])
            bot.send_message(send_to, f"<a href='tg://user?id={message.from_user.id}'>{user_name}</a>" + ", ваш рейтинг: " +  str(z[5]) + " 👑", parse_mode='html')
    elif message.text.lower() in ["Профиль", "профиль"]:
        ud = userdata(message.from_user.id)
        for u in ud:
            user_name = str(u[1])
            bot.send_message(send_to, f"<a href='tg://user?id={message.from_user.id}'>{user_name}</a>" + ", ваш профиль:" + "\n✍ ID: " +  str(u[0]) + "\n💰 Вареников: " + str(u[2]) + "\n🏦 В пельменной: " + str(u[4]) +
                             "\n👑 Рейтинга: " + str(u[5]) + "\n📃 Статус: " + str(u[3]) + "\n📦 Кейсов: " + str(u[6]) + "\nВсё остальное пока что в разработке 😋", parse_mode='html')
            userproverkacoins(message.from_user.id)
    elif message.text.lower() in ["Статус", "статус"]:
        us = userdata(message.from_user.id)
        for s in us:
            bot.send_message(send_to, "@" + str(message.from_user.username) + ", ваш статус: " + str(s[3]))
            userproverkacoins(message.from_user.id)
    elif message.text.lower() in ["/help", "помощь", "Помощь"]:
        keyboard = types.InlineKeyboardMarkup()
        key_about = types.InlineKeyboardButton(text='Обо мне', callback_data="about")
        keyboard.add(key_about)
        key_games = types.InlineKeyboardButton(text='Игровые', callback_data="games")
        keyboard.add(key_games)
        key_stats = types.InlineKeyboardButton(text='Статистика', callback_data="stats")
        keyboard.add(key_stats)
        key_different = types.InlineKeyboardButton(text='Разное', callback_data="different")
        keyboard.add(key_different)
        key_admin = types.InlineKeyboardButton(text='Админ', callback_data="admin")
        keyboard.add(key_admin)
        bot.send_message(send_to, text='Выбери, в чём ты хочешь разобраться', reply_markup=keyboard)
        userproverkacoins(message.from_user.id)


@bot.callback_query_handler(func=lambda call: True)
def callback_worker(call):
    send_to = call.message.chat.id
    if call.data == "about":
        bot.send_message(send_to, "Меня зовут PGBot. Я пока что в процессе разработки. Меня создал @XxfloppaxX. Приятно познакомиться) Спасибо что выбрал меня!")
    if call.data == "games":
        bot.send_message(send_to, "Игровые команды:\nСварить пельмени (ставка) (можно сразу все, пиши \"Сварить пельмени всё\"\nКупить кейс (кол-во)\nОткрыть кейс"
                                  "\nЕсли вы нашли какую либо ошибку или хотите предложить добавить какую-либо команду  - обращайтесь к @XxfloppaxX ")
    if call.data == "stats":
        bot.send_message(send_to, "Команды-статистика:\nРегистрация\nУдалиться\nСменить ник (не доделана)\nБ/Баланс\nПрофиль\nАйди (можно посмотреть свое либо чужое)"
                                  "\nЕсли вы нашли какую либо ошибку или хотите предложить добавить какую-либо команду  - обращайтесь к @XxfloppaxX ")
    if call.data == "different":
        bot.send_message(send_to, "Разное:\nНасколько я (к примеру яблоко)\nКогда (например я стану отличником)\nШанс\nКупить/продать рейтинг (1 рейтинг = 150000 вареников)\nДать (сумма)"
                                  "\nПередать (сумма) (айди)\nРейтинг\nСтатус\nБанк положить/снять (сумма)"
                                  "\nЕсли вы нашли какую либо ошибку или хотите предложить добавить какую-либо команду  - обращайтесь к @XxfloppaxX ")
    if call.data == "admin":
        bot.send_message(send_to, "Админ-команды:\nВыдать\nЗабрать\nОбнулить\nЕсли вы нашли какую либо ошибку, хотите предложить добавить какую-либо команду или купить админа  - обращайтесь к @XxfloppaxX.")


while True:
    try:
        bot.polling(none_stop=True)
    except:
        continue

